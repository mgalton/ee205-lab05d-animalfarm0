///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// Usage:  updateCats
///
/// Result:
///   Updates cats in database
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   22_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#pragma once

int updateCatName (const int index, const char* newName);

int fixCat(const int index);

int updateCatWeight(const int index,const float newWeight);
