///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// Usage:  catDatabase
///
/// Result:
///   contains data of the cats
///
/// @file catDatabase.c
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   22_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

char maxcatname[MAX_CAT_NUM] [MAX_CAT_NAME];
enum Gender catgender[MAX_CAT_NUM];               // redeclaring Gender as catgender
enum Breed catbreed[MAX_CAT_NUM];                 // redeclaring Breed as catbreed
bool fixed[MAX_CAT_NUM];                         
float catweight[MAX_CAT_NUM];

int countcat = 0;       //Initiallizing zero as the starting point
