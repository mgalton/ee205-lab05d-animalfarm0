///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// Usage:  catDatabase
///
/// Result:
///   contains data of the cats
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   22_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#pragma once
#define MAX_CAT_NUM 10
#define MAX_CAT_NAME 32



enum Gender {UNKNOWN_GENDER, MALE, FEMALE};
enum Breed {UNKOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

extern   char     maxcatname[MAX_CAT_NUM] [MAX_CAT_NAME];
extern   int      countcat;
extern   enum Gender catgender[MAX_CAT_NUM];
extern   enum Breed  catbreed[MAX_CAT_NUM];
extern   bool        fixed[MAX_CAT_NUM];
extern   float       catweight[MAX_CAT_NUM];





