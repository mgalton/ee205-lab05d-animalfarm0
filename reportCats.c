///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// Usage:  reportCats
///
/// Result:
///   Prints the cats in the database
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   22_Feb_2022
//////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdbool.h>
#include "reportCats.h"
#include "catDatabase.h"
#include <stdlib.h>
#include <string.h>

int printCat(int index) {

   if (index < 0 || index > MAX_CAT_NUM) {
      printf("animalFarm0: Bad cat [%d]\n", index);
      return 0;

   }

   else {
      printf("cat index = [%u], name = [%s], gender = [%d], breed = [%d], isFixed = [%d], weight = [%f]\n", index, maxcatname[index], catgender[index], catbreed[index], fixed[index], catweight[index]);
   };

   return 1;
}

int printAllCats(){

   for (int i = 0; i < countcat; i++) {
       printf("cat index = [%u], name = [%s], gender = [%d], breed = [%d], isFixed = [%d], weight = [%f]\n", i, maxcatname[i], catgender[i], catbreed[i], fixed[i], catweight[i]);

}
   return 0;
}


int findCat(char name[]){
   int i = 0;

   while (i < MAX_CAT_NUM) {
      if (name == maxcatname[i]){

      printf("The name of the cat is %s\n", maxcatname[i]);
   }

   i++;

   }
   return 0;
}

