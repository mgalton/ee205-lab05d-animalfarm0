///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// Usage:  updateCats
///
/// Result:
///   Updates cat info in the database
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   22_Feb_2022
//////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "catDatabase.h"
#include "updateCats.h"
#include <string.h>
#include <stdlib.h>

int updateCatName(const int index,const char newName[]){

strcpy(maxcatname[index], newName);
   printf("Cat name has been changed to %s \n", newName);

   return 1;
}

int fixCat (int index){
   if (fixed == 0){
      printf("Updated: %s is fixed\n", maxcatname[index]);
   }
   else {
      printf("Update Failure: No updates were made\n");
      return 0;
   }
   return 1;
}

int updateCatWeight(int index,float newWeight){
   if (newWeight > 0) {
      catweight[index] = newWeight;
      printf("Cat weight has been updated to %f\n", newWeight);
   }
   else {
      printf("Error: Cat weight must be greater than 0\n");
      return 0;
   }
   return 1;
}

   
