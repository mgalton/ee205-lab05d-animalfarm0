///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// Usage:  addCats
///
/// Result:
///   Adds cats to database
///
/// @file addCats.c
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   22_Feb_2022
///////////////////////////////////////////////////////////////////////////////


#include "addCats.h" 
#include <stdio.h>
#include <stdbool.h>
#include "string.h"
#include "catDatabase.h"
#include <stdlib.h>

int addCat(const char name[],const int gender,const int breed,const bool isFixed,const float weight) {

   if (strlen(name) >= MAX_CAT_NAME){
      printf("Name of cat cannot be greater than %d letters\n", MAX_CAT_NAME);
      return 0;
   }

   if (strlen(name) == 0) {
      printf("Cat name must be entered\n");
      return 0;
   }
  
   
   if (weight <= 0) {
      printf("Cat weight must be entered\n");
      return 0;
   }

   if ( countcat > MAX_CAT_NUM) {
      printf("Too many cats. Only %d cats can be in the database at the moment\n", MAX_CAT_NUM);
      return 0;
   }

   strncpy( maxcatname[countcat], name, MAX_CAT_NAME);
   catgender[countcat] = gender;
   catbreed[countcat] = breed;
   fixed[countcat] = isFixed;
   catweight[countcat] = weight;
   
   countcat++;
   
   return 1;
}
