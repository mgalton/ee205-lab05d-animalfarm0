///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    main.c
/// @version 1.0 - Initial version
///
/// @author  Mariko Galton <mgalton@hawaii.edu>
/// @date    22_Feb_2022
///
/// @see     https://en.wikipedia.org/wiki/%22Hello,_World!%22_program
/// @see     https://www.thesoftwareguild.com/blog/the-history-of-hello-world/
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include "addCats.h"
#include "catDatabase.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"

int main() {

addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
addCat( "Milo", MALE, MANX, true, 7.0 ) ;
addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;

printAllCats();

int kali = findCat( "Kali" ) ;
updateCatName( kali, "Chili" ) ; 
printCat( kali );
updateCatName( kali, "Capulet" ) ;
updateCatWeight( kali, 9.9 ) ;
fixCat( kali ) ;
printCat( kali );

printAllCats();

deleteAllCats();
printAllCats();

}




