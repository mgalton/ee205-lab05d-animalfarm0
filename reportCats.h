///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// Usage:  reportCats
///
/// Result:
///   finds cats and prints the database
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   22_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#pragma once

extern int printCat(int index);
extern int printAllCats();
extern int findCat(char name[]);

