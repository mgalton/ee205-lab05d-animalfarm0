///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// Usage:  deleteCats
///
/// Result:
///   Adds cats to database
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Mariko Galton  <mgalton@hawaii.edu>
/// @date   22_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "deleteCats.h"
#include "catDatabase.h"

int deleteAllCats() {
   countcat = 0;
   return 1;
}
